import { computed, ref } from "vue"
import { useAxios } from "@/composables/useAxios.composable"
import { useExperimentStore } from "@/stores/useExperimentStore"
import type { Error } from "#/error"

export function useExperiment() {
	const experiment = ref<number>(1)
	const step = ref<number>(1)
	const store = useExperimentStore()
	const sessionID = ref<string>(store.sessionID)

	const experimentId = computed(() => experiment.value)
	const stepId = computed(() => step.value)

	const firstStep = async () => {
		const { axios } = useAxios()

		const token = localStorage.getItem("token")
		if (!token) {
			throw new Error("No token")
		}
		try {
			await axios.post(
				"/stat/progress/" + sessionID.value,
				{
					experiment: experiment.value,
					step: step.value,
				},
				{
					headers: {
						Authorization: `Bearer ${token}`,
					},
				}
			)
		} catch (e) {
			const { response } = e as { response: { data: Error } }
			throw new Error(response.data.message)
		}
	}

	const nextStep = async () => {
		step.value += 1

		const { axios } = useAxios()

		const token = localStorage.getItem("token")
		if (!token) {
			throw new Error("No token")
		}
		try {
			await axios.patch(
				"/stat/progress/" + sessionID.value,
				{
					experiment: experiment.value,
					step: step.value,
				},
				{
					headers: {
						Authorization: `Bearer ${token}`,
					},
				}
			)
		} catch (e) {
			const { response } = e as { response: { data: Error } }
			throw new Error(response.data.message)
		}
	}

	const prevStep = () => {
		step.value -= 1
	}

	const nextExperiment = () => {
		experiment.value += 1
	}
	return {
		experiment,
		step,
		experimentId,
		stepId,
		nextStep,
		prevStep,
		nextExperiment,
		firstStep,
	}
}
