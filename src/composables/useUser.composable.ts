import type { User, UserLogin, UserLoginBody, UserMailRole } from "#/user"
import { useAxios } from "@/composables/useAxios.composable"
import type { Error } from "#/error"

enum Role {
	ADMIN = "admin",
	TEACHER = "teacher",
	INSTRUCTOR = "instructor",
	STUDENT = "student",
} // sort by priority

interface loginReturn {
	valid: boolean
	error: Error | null
}

const useUser = () => {
	const { axios } = useAxios()

	const login = async (userLoginBody: UserLoginBody) => {
		try {
			// data is the response body from the server to the request post to /user/login with the body userLoginBody ( { email, password } )
			const { data } = (await axios.post<UserLogin>("/user/login", { ...userLoginBody })) as { data: UserLogin }
			// set the token, expiresAt, role and id in the localStorage
			localStorage.setItem("token", data.token)
			localStorage.setItem("expiresAt", data.expiresAt)
			localStorage.setItem("role", data.role)
			localStorage.setItem("id", data.id)
			return { valid: true, error: null }
		} catch (e) {
			const { message, name } = e as { message: string; name: string }
			if (message && name.includes("Axios")) {
				return { valid: false, error: { message } }
			} else {
				const { response } = e as { response: { data: Error } }
				return { valid: false, error: response.data }
			}
		}
	}

	const signupInvited: (pseudo: string) => Promise<UserLogin> = async (pseudo: string) => {
		const { data } = (await axios.post<UserLogin>("/user/signup", {
			name: pseudo,
			isInvited: true,
		})) as { data: UserLogin }

		localStorage.setItem("id", data.id)

		return data
	}

	const loginInvited = async (pseudo: string) => {
		try {
			const dataSignup = await signupInvited(pseudo)
			// data is the response body from the server to the request post to /user/login with the body userLoginBody ( { email, password } )
			const { data } = (await axios.post<UserLogin>("/user/login", {
				id: dataSignup.id,
				isInvited: true,
			})) as { data: UserLogin }
			// set the token, expiresAt, role and id in the localStorage
			localStorage.setItem("token", data.token)
			localStorage.setItem("expiresAt", data.expiresAt)
			localStorage.setItem("role", data.role)

			return { valid: true, error: null }
		} catch (e) {
			const { message, name } = e as { message: string; name: string }
			if (message && name.includes("Axios")) {
				return { valid: false, error: { message } }
			} else {
				const { response } = e as { response: { data: Error } }
				return { valid: false, error: response.data }
			}
		}
	}
	const isBetterThan = (role1: Role, role2: Role) => {
		const roles = [Role.ADMIN, Role.TEACHER, Role.INSTRUCTOR, Role.STUDENT]
		return roles.indexOf(role1) <= roles.indexOf(role2)
	}
	const canUpdateUserRole = (token: string | null, user: UserMailRole) => {
		console.log("canUpdateUserRole")
		if (token === null) return false
		console.log("token is not null")
		const role = localStorage.getItem("role")
		if (role === null) return false
		console.log("role is not null")
		if (isBetterThan(role as Role, user.role)) {
			console.log("role is better than user.role")
			return true
		} else {
			console.log("role is not better than user.role")
			return false
		}
	}

	const updateRole = async (token: string, user: UserMailRole) => {
		try {
			const { data } = (await axios.put<UserMailRole>(
				"/user/update/role",
				{ ...user },
				{
					headers: {
						Authorization: `Bearer ${token}`,
					},
				}
			)) as { data: UserMailRole }
			return { valid: true, error: data }
		} catch (e) {
			const { response } = e as { response: { data: Error } }
			return { valid: false, error: response.data }
		}
	}
	const cleanUserLocalStorage = () => {
		// Clean user data from local storage
		localStorage.removeItem("token")
		localStorage.removeItem("expiresAt")
		localStorage.removeItem("role")
		localStorage.removeItem("id")
	}

	const getAllUsers = async (token: string) => {
		const result = (await axios.get("/user/all", {
			headers: {
				Authorization: `Bearer ${token}`,
			},
		})) as { data: User[] }
		return result.data
	}
	return { login, canUpdateUserRole, cleanUserLocalStorage, getAllUsers, updateRole, loginInvited }
}

export { useUser, Role }
export type { loginReturn }
