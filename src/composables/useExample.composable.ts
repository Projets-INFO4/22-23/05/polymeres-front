import { computed, ref } from "vue"
import type { Example } from "@/../../types/example"

export function useExample() {
	const example = ref<Example>({ id: 1, name: "Example" })
	const exampleName = computed(() => example.value.name)
	const exampleId = computed(() => example.value.id)
	const exampleNameLength = computed(() => example.value.name.length)

	const setExampleName = (newName: string) => {
		example.value.name = newName
	}

	return {
		example,
		exampleName,
		exampleId,
		exampleNameLength,
		setExampleName,
	}
}
