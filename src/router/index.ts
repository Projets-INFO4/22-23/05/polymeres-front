import { createRouter, createWebHistory } from "vue-router"
import HomeView from "../views/HomeView.vue"
import { useAxios } from "@/composables/useAxios.composable"
import { useUser } from "@/composables/useUser.composable"

const { axios } = useAxios()
const { cleanUserLocalStorage } = useUser()

const router = createRouter({
	history: createWebHistory(import.meta.env.BASE_URL),
	routes: [
		{
			path: "/",
			name: "home",
			component: HomeView,
		},
		{
			path: "/about",
			name: "about",
			// route level code-splitting
			// this generates a separate chunk (About.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () => import("../views/AboutView.vue"),
		},
		{
			path: "/login",
			name: "login",
			component: () => import("../views/LoginView.vue"),
		},
		{
			path: "/loginInvited",
			name: "loginInvited",
			component: () => import("../views/LoginInvitedView.vue"),
		},
		{
			path: "/admin",
			name: "admin",
			component: () => import("../views/AdminView.vue"),
			meta: {
				requiresAuth: true,
			},
		},
		{
			path: "/logout",
			name: "logout",
			component: () => import("../views/LogoutView.vue"),
		},
		{
			path: "/end",
			name: "end",
			component: () => import("../views/EndView.vue"),
		},
		{
			path: "/feedback",
			name: "feedback",
			component: () => import("../views/FeedbackView.vue"),
		},
		{
			path: "/help",
			name: "help",
			component: () => import("../views/HelpView.vue"),
		},
		{
			path: "/finalPassword",
			name: "finalPassword",
			component: () => import("../views/FinalPasswordView.vue"),
		},
		{
			path: "/password",
			name: "password",
			component: () => import("../views/PasswordView.vue"),
		},
		{
			path: "/experiment",
			name: "experiment",
			component: () => import("../views/ExperimentView.vue"),
		},
		{
			path: "/choice",
			name: "choice",
			component: () => import("../views/ChoiceExpView.vue"),
			meta: {
				requiresAuth: true,
			},
		},
	],
})

const isValid = async (token: string) => {
	// Check if the token is valid and if it allows the user to access the route
	if (token === null) {
		return false
	}
	const result = await axios.get("/user/verify/adminPanel", {
		headers: {
			Authorization: `Bearer ${token}`,
		},
	})
	switch (result.status) {
		case 200:
			console.log("Your session is valid")
			return true
		case 400:
			alert("Your session is not valid")
			return false
		case 401:
			alert("You are not allowed to access this page")
			return false
		default:
			alert("An error occurred")
			return false
	}
}

router.beforeEach((to, from, next) => {
	const requiresAuth = to.matched.some(record => record.meta.requiresAuth) // Check if the route requires authentication
	const token = localStorage.getItem("token")
	const expires_at = localStorage.getItem("expiresAt")

	if (requiresAuth === false || requiresAuth === undefined) {
		// If the route does not require authentication, continue
		return next()
	}

	if (!token || !expires_at) {
		cleanUserLocalStorage()
		alert("You need to login first")
		if (to.name === "choice") {
			return next("/loginInvited")
		}
		if (to.name === "admin") {
			// If the token does not exist, clean the local storage and redirect to login
			cleanUserLocalStorage()
			alert("You need to login first")
			return next("/login")
		} else {
			return next()
		}
	} else if (token && expires_at < Date.now().toString()) {
		// If the token is expired, clean the local storage and redirect to login
		cleanUserLocalStorage()
		alert("Your session has expired")
		return next("/login")
	} else if (to.name != "choice" && !isValid(token)) {
		// If the token is not valid, clean the local storage and redirect to login
		cleanUserLocalStorage()
		alert("Your session is not valid")
		return next("/login")
	} else {
		// If the token is valid, continue
		return next()
	}
})

export default router
