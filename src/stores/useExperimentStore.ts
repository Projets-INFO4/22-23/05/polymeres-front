import { ref } from "vue"
import { defineStore } from "pinia"
import type { ProgressStatus, ProgressStatusApiResult } from "#/progress"
import { useAxios } from "@/composables/useAxios.composable"

export const useExperimentStore = defineStore("experiment", () => {
	const progressStatus = ref<ProgressStatus[]>([])
	const sessionID = ref<string>("c5fa2bff-80ba-483b-a158-0d3ae4a32558") // TODO: When session are implemented, this should be changed to adapt to the user's session
	const experimentID = ref<number>(1)
	const step = ref<number>(1)

	const loadProgressStatus = async () => {
		const { axios } = useAxios()

		const token = localStorage.getItem("token")
		if (!token) {
			throw new Error("No token")
		}

		const { data } = (await axios.get<ProgressStatusApiResult[]>("/stat/progress/status/" + sessionID.value, {
			headers: {
				Authorization: `Bearer ${token}`,
			},
		})) as { data: ProgressStatusApiResult[] }

		progressStatus.value = data.map(d => ({
			status: d.endTime ? "success" : "pending",
			user: d.User.name,
			experiment: d.experiment,
			step: d.step,
			startTime: new Date(d.startTime),
			endTime: d.endTime ? new Date(d.endTime) : null,
			startStepTime: new Date(d.startStepTime),
		}))
	}

	return { progressStatus, loadProgressStatus, sessionID, experimentID, step }
})
