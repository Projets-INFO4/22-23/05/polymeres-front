import { ref } from "vue"
import { defineStore } from "pinia"
import type { SetOfSetOfExperience } from "@/../../types/experience"

export const useExperienceStore = defineStore("experiences", () => {
	const setOfSetOfExperience = ref<SetOfSetOfExperience | null>({
		setOfExperiences: [
			{
				title: "Premier set d'expérience",
				experiences: [
					{
						title: "Eprouvette",
						instructions: ["Munissez-vous de la boîte portant l’étiquette “Eprouvettes. A l’intérieur se trouve le matériel dont vous aurez besoin pour l’expérience.", "rébus 1", "rébus 2"],
					},
					{
						title: "Alginate",
						instructions: [
							"Munissez-vous de la boîte portant l’étiquette “Alginate”. A l’intérieur se trouve le matériel dont vous aurez besoin pour l’expérience.",
							"Les fioles dont vous disposez contiennent 5 g de poudre d’alginate. Vous êtes entouré de détecteurs à particule d’alginate qui seront reliés à une alarme. Pour ne pas alerter Maurice Evannt et retourner dans votre cellule vous ne devez absolument pas sortir la poudre du tube.",
							"Vous allez donc devoir y ajouter l’une des solutions dont vous disposez et provoquer une réticulation afin de pouvoir vider le tube.",
							"Vous disposez d’un flacon d’huile, d’eau salée et d’eau pure.\n" +
								"Vous savez qu’il faut ?? mL de solution pour 1g de poudre, à vous de calculer le volume nécessaire pour votre epérience\n",
							"Enigme pour le faire déviner qu’il faut utiliser l’eau, affinité des molécules ?\n",
							"Enigme pour calculer la quantité d’eau -> 22 mg," + "",
							"Alginate : (C6H8O6)n    \n" + "\n" + " Un code se trouve au fond de la fiole. Récupérez le. \n",
						],
					},
					{
						title: "Viscosité",
						instructions: ["Munissez-vous de la boîte portant l’étiquette “Viscosité”. A l’intérieur se trouve le matériel dont vous aurez besoin pour l’expérience.", "rébus 1", "rébus 2"],
					},
				],
			},
			{
				title: "Set d'éxpérience Pierre 2",
				experiences: [
					{
						title: "Expérience Pierre 21",
						instructions: ["instruction 1", "instruction 2", "instruction 3"],
					},
					{ title: "Expérience Pierre 26", instructions: ["instruction 1", "instruction 2", "instruction 3"] },
				],
			},
		],
	})
	const step = ref<number>(0)
	const experience = ref<number>(0)
	const setOfExperience = ref<number>(0)
	const actualInstruction = ref<string>("")
	const actualExperience = ref<string>("")

	const prevStep = () => {
		console.log("prevStep called : " + step.value)
		if (step.value === 0) {
			alert("You are in the first step")
		} else {
			step.value--
			refresh()
			console.log(
				"prevStep decremented : " +
					step.value +
					" and actualInstruction set to " +
					actualInstruction.value +
					" and actualExperience set to " +
					actualExperience.value +
					" and setOfExperience set to " +
					setOfExperience.value
			)
		}
	}

	const nextStep = () => {
		if (
			setOfSetOfExperience.value?.setOfExperiences[setOfExperience.value].experiences[experience.value].instructions != null &&
			step.value != setOfSetOfExperience.value?.setOfExperiences[setOfExperience.value].experiences[experience.value].instructions.length - 1
		) {
			step.value++
			actualInstruction.value = setOfSetOfExperience.value?.setOfExperiences[setOfExperience.value].experiences[experience.value].instructions[step.value] || ""
			refresh()
			console.log(
				"nextStep incremented : " +
					step.value +
					" and actualInstruction set to " +
					actualInstruction.value +
					" and actualExperience set to " +
					actualExperience.value +
					" and setOfExperience set to " +
					setOfExperience.value +
					" and experience set to " +
					experience.value
			)
		} else {
			console.log("You have finished all the steps you can go to the next experience")
			nextExperience()
		}
		// TODO: Update progress on database
	}

	const nextExperience = () => {
		console.log("nextExperience called : " + experience.value)
		if (
			setOfSetOfExperience.value?.setOfExperiences[setOfExperience.value].experiences != null &&
			experience.value != setOfSetOfExperience.value?.setOfExperiences[setOfExperience.value].experiences.length - 1
		) {
			experience.value++
			step.value = 0
			refresh()
			console.log("nextExperience incremented : " + experience.value + " and step reset to 0" + step.value)
		} else if (
			setOfSetOfExperience.value?.setOfExperiences[setOfExperience.value].experiences != null &&
			experience.value === setOfSetOfExperience.value?.setOfExperiences[setOfExperience.value].experiences.length - 1
		) {
			if (setOfSetOfExperience.value?.setOfExperiences != null && setOfExperience.value != setOfSetOfExperience.value?.setOfExperiences.length - 1) {
				setOfExperience.value++
				experience.value = 0
				step.value = 0
				refresh()
				console.log("nextExperience incremented : " + experience.value + " and step reset to 0" + step.value)
			} else {
				alert("You have finished all the experiences")
			}
		}
		// TODO: Update progress on database
	}

	const refresh = () => {
		actualExperience.value = setOfSetOfExperience.value?.setOfExperiences[setOfExperience.value].experiences[experience.value].title || ""
		actualInstruction.value = setOfSetOfExperience.value?.setOfExperiences[setOfExperience.value].experiences[experience.value].instructions[step.value] || ""
	}

	refresh()

	return {
		setOfSetOfExperience,
		step,
		experience,
		actualInstruction,
		nextStep,
		nextExperience,
		prevStep,
		actualExperience,
	}
})
