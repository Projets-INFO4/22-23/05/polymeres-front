interface ProgressStatus {
	status: "pending" | "success"
	user: string
	experiment: number
	step: number
	startTime: Date
	endTime: Date | null
	startStepTime: Date
}

interface ProgressStatusApiResult {
	experiment: number
	step: number
	startTime: string
	endTime: string | null
	startStepTime: string
	User: { name: string }
}

export { ProgressStatus, ProgressStatusApiResult }
