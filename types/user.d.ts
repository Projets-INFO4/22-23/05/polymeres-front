import { Role } from "@/composables/useUser.composable"

interface UserLogin {
	id: string
	role: Role
	expiresAt: string
	token: string
}

interface UserLoginBody {
	email: string
	password: string
}

interface User {
	id: string
	email: string
	password: string
	name: string
	role: Role
}

interface UserMailRole {
	email: string
	role: Role
}

export type { User, UserLogin, UserLoginBody, UserMailRole }
