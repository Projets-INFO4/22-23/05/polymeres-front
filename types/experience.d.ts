interface SetOfExperience {
	title: string
	experiences: Experience[]
}

interface Experience {
	title: string
	instructions: string[]
}

interface SetOfSetOfExperience {
	setOfExperiences: setOfExperience[]
}

export { SetOfExperience, Experience, SetOfSetOfExperience }
