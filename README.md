# Polymeres frontend

- [Installation](#installation)
- [Install Node.js using NVM (Node Version Manager) is recommended.](#--install-nodejs-using-nvm-node-version-manager-is-recommended)
- [Development](#development)
- [Production](#production)
- [Linter](#linter)
- [Husky](#husky)
- [Environment variables](#environment-variables)
- [How to contribute](#how-to-contribute)
- [Continuous Integration (CI) with Gitlab](#continuous-integration-ci-with-gitlab)
- [Contributors](#contributors)

## Installation

### Clone the repository

```bash
git clone <repository>
```

### Install Node.js and npm

- You can find the installation instructions for Node.js on the official
  website: [Node.js](https://nodejs.org/en/download/)
- Use the LTS version.
- Install Node.js using NVM (Node Version Manager) is recommended.
-

### Install the dependencies with npm (first time only or when dependencies change)

```bash
npm install
```

## Development

### Launch the development server and the database

```bash
cd backend
npm run init-db
npm run dev
```

### Start the server in development mode

```bash
cd frontend
npm run dev
```

- The server will be available at http://localhost:8080
- The API will be available at http://localhost:3000
- The database will be available at http://localhost:5432

### Test with Vitest

```bash 
npm run test
```

```bash
 npm run test -- --coverage
```

## Production

### Build the frontend

```bash
cd frontend
npm run build
```

- Host the files in the `dist` folder on a web server.

## Linter

```bash
npm run lint
```

- We use [ESLint](https://eslint.org/) to lint the frontend and [Prettier](https://prettier.io/) to format the code.
- The configuration is in the `.eslintrc.cjs` file and `prettier.json` file.

## Husky

- Husky is a tool that allows you to run scripts when you commit or push your code
- The scripts are defined in the directory `.husky`
- You can find more information about Husky here: [Husky](https://typicode.github.io/husky/#/)
- There is a pre-commit hook that runs the linting

## Environment variables

- The environment variables are in the `.env` file.
- The environment variables are prefixed with `VITE_` to be accessible in the frontend.
- The environment variables are :
    - `VITE_API_URL` : the URL of the API

---

## How to contribute

### Install git flow

- You can find the installation instructions for git flow on the web
- This tool is used to manage the branches and the releases of the project
- You can find more information about git flow
  here: [git flow](https://www.atlassian.com/fr/git/tutorials/comparing-workflows/gitflow-workflow#:~:text=Qu'est%2Dce%20que%20Gitflow,Vincent%20Driessen%20de%20chez%20nvie.)
- The default configuration is used
- The default branch is `develop`
- The default prefix for the branches is `feature/`, `bugfix/`, `release/` and `hotfix/`
- The default prefix for the tags is `v`
- The first time you use git flow, you need to initialize it with the command `git flow init`

### Create a branch for the new feature

- You need to create a new branch for the new feature you want to add to the project
- You can use the command `git flow feature start <feature_name>`

### Create a component

- You can create a new component in the `src/components` folder in typescript
- You must name the component with the PascalCase convention and the `.vue` extension (e.g. `MyComponent.vue`)

### Create composable and store (if needed)

- You can create a new composable in the `src/composables` folder in typescript
- You can create a new store in the `src/store` folder in typescript
- You must name the composable and the store with the PascalCase convention and the `.ts` extension (
  e.g. `MyComposable.ts`)
- Store are used to store the data of the application
- Composables are used to store the logic of the application
- You can find more information about the difference between store and composables
  here: [Store vs Composables](https://v3.vuejs.org/guide/composition-api-introduction.html#why-composition-api)

### Create a view

- You can create a new page in the `src/view` folder in typescript
- You must name the page with the PascalCase convention and the `.vue` extension (e.g. `MyPage.vue`)

### Add a new route

- You can add a new route in the `src/router/index.ts` file
- You can find more information about the router
  here: [Vue Router](https://router.vuejs.org/guide/#html)

### Create the tests

- You can create the tests for the new component in the `src/components/__tests__` folder
- You can create the tests for the new composable in the `src/composables/__tests__` folder
- You can create the tests for the new store in the `src/store/__tests__` folder
- You can find more information about the tests
  here: [Vitest](https://vitest.dev/)

### Merge Request

- You need to create a merge request to merge your branch into the branch `develop`
- A merge request is created on the web interface of gitlab or github
- You can find more information about merge request here:
    - [Gitlab](https://docs.gitlab.com/ee/user/project/merge_requests/)
    - [Github](https://docs.github.com/en/github/collaborating-with-issues-and-pull-requests/about-pull-requests)

### Finish the feature

- You need to finish the feature when the merge request is accepted
- You can use the command `git flow feature finish <feature_name>`
- You can use the command `git push --all` to push all the branches to the remote repository

### Create a release

- You need to create a release when the merge request is accepted
- You can use the command `git flow release start <release_name>`
- You have to update the version of the project in the file `package.json` and possibly in
  other files
- You can use the command `git flow release finish <release_name>`
- You can use the command `git push --all` to push all the branches to the remote repository
- You can use the command `git push --tags` to push all the tags to the remote repository

---

## Continuous Integration (CI) with Gitlab

- We use Gitlab to manage the project
- We use Gitlab CI to run the tests and the linting
- You can find more information about Gitlab CI here: [Gitlab CI](https://docs.gitlab.com/ee/ci/)
- The configuration of the CI is in the file `.gitlab-ci.yml`
- The CI is run when you push your code on the remote repository
- The CI is run on the branch `develop` and on `main`
- The CI is run on the following stages:
    - `lint`
    - `test`
    - `build`

---

## Contributors

- Romain Guyot
- Aurélie Aventurier
- Lilian Soler 









